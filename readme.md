# Globalist Agenda Watch

Identifies websites promoting the globalist agenda in your DuckDuckGo search results.

## Build locally

Make sure you have NodeJS 16 installed. Then:

* run `npm install` to install all required dependencies
* run `npm run dist`

The `ext.zip` file will be in the `target` directory.

## Development

Make sure you have [web-ext](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext/) installed globally. Then:

* run `npm run watch` to watch for file changes and build continuously
* in another terminal, run `web-ext run` for Firefox or `web-ext run -t firefox-desktop`

## Credits

The extension icon was created by [Kiranshastry](https://www.flaticon.com/authors/kiranshastry) and downloaded from [Flaticon](https://www.flaticon.com/).

## License

This browser extension is released under GNU GPL3.

[![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
