import { retrieveOptions } from './options-storage.js';
import { domainUpdateNeeded, updateDomains } from './options-util.js';

async function init() {
  const options = await retrieveOptions();
  const storedUpdateUrl = options.updateUrl;
  const storedLastUpdated = options.lastUpdated;
  const errorMessage = options.errorMessage;
  if (domainUpdateNeeded(errorMessage, storedLastUpdated, storedUpdateUrl)) {
    updateDomains();
  }
}

init();
