import $ from 'cash-dom';
import { retrieveOptions } from './options-storage.js';
import { updateDomains } from './options-util.js';

const updateUrlInput = $('#update-url');
const updateButton = $('#update-button');
const lastUpdated = document.querySelector('#last-updated');

function formatLastUpdatedMessage(lastUpdatedDate) {
  const formatted = lastUpdatedDate.toLocaleString();
  return `Successfully updated: ${formatted}`;
}

function updateStatus(text) {
  lastUpdated.innerText = text;
}

updateButton.on('click', async () => {
  const updateUrl = updateUrlInput.val();
  const [lastUpdatedDate, errorMessage] = await updateDomains(updateUrl);
  if (!errorMessage) {
    updateStatus(formatLastUpdatedMessage(lastUpdatedDate));
  } else {
    updateStatus(errorMessage);
  }
});

async function init() {
	const options = await retrieveOptions();
  const storedUpdateUrl = options.updateUrl;
  const errorMessage = options.errorMessage;
  updateUrlInput.val(storedUpdateUrl);
  if (!errorMessage) {
    const storedLastUpdated = options.lastUpdated;
    const lastUpdatedDate = new Date(storedLastUpdated);
    updateStatus(formatLastUpdatedMessage(lastUpdatedDate));
  } else {
    updateStatus(errorMessage);
  }
}

init();
