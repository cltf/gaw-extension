import { storeOptions, retrieveOptions } from './options-storage.js';

const updateThreshold = 86400000;
const extensionUpdateUrl = "https://cltf.codeberg.page/gaw/domains.json";

async function fetchDomains(url) {
  const response = await fetch(url, {
    method: 'GET',
    mode: 'no-cors',
    headers: {
      'Content-Type': 'application/json',
    }
  });
  return response.json();
}

export function domainUpdateNeeded(errorMessage, storedLastUpdated, storedUpdateUrl) {
  const nowDate = new Date().valueOf();
  return errorMessage || !storedLastUpdated || extensionUpdateUrl != storedUpdateUrl ||
    (nowDate - storedLastUpdated > updateThreshold);
}

export async function updateDomains() {
  let updateUrl;
  let domains;
  let errorMessage;
  let lastUpdatedDate;
  try {
    updateUrl = extensionUpdateUrl;
    domains = await fetchDomains(updateUrl);
    errorMessage = null;
    lastUpdatedDate = new Date();
  } catch (e) {
    const options = await retrieveOptions();
    updateUrl = options.updateUrl;
    domains = null;
    errorMessage = 'Error occurred while fetching updated data!';
    const storedLastUpdated = options.lastUpdated;
    lastUpdatedDate = new Date(storedLastUpdated);
  }
  await storeOptions({
    lastUpdated: lastUpdatedDate.valueOf(),
    domains,
    updateUrl,
    errorMessage
  });
  return [lastUpdatedDate, errorMessage];
}
