import { retrieveOptions } from './options-storage.js';

function highlightResult(urlElement) {
  const domainRatingElement = document.createElement('span');
  domainRatingElement.classList.add('pwe__highlight');
  domainRatingElement.innerText = '0';
  urlElement.prepend(domainRatingElement);
}

function matchDomain(dataDomain, urlElement, { plain, regex }) {
  const plainIndex = plain.indexOf(dataDomain);
  if (plainIndex !== -1) {
    highlightResult(urlElement);
  } else {
    const matched = regex.find(r => r.test(dataDomain));
    if (matched) {
      highlightResult(urlElement);
    }
  }
}

function decorateResultsJs(domains) {
  const results = document.querySelectorAll('div.result.results_links_deep');
  results.forEach(resultElement => {
    const dataDomain = resultElement.getAttribute('data-domain');
    const urlElement = resultElement.querySelector('div.result__extras__url');
    const alreadyDecorated = urlElement.querySelector('span.pwe__highlight') !== null;
    if (!alreadyDecorated) {
      matchDomain(dataDomain, urlElement, domains);
    }
  });
}

function decorateResultsHtml(domains) {
  const extractDomainRegex = /\/\/([^/]+).*/;

  function extractDomain(urlElement) {
    if (urlElement) {
      const anchorElement = urlElement.querySelector('a.result__url');
      if (anchorElement) {
        const href = anchorElement.getAttribute('href');
        const domainData = extractDomainRegex.exec(href);
        if (Array.isArray(domainData) && domainData.length === 2) {
          return domainData[1];
        }
      }
    }
    return null;
  }

  const results = document.querySelectorAll('div.result.results_links_deep');
  results.forEach(resultElement => {
    const urlElement = resultElement.querySelector('.result__extras');
    const dataDomain = extractDomain(urlElement);
    const alreadyDecorated = urlElement.querySelector('span.pwe__highlight') !== null;
    if (!alreadyDecorated) {
      matchDomain(dataDomain, urlElement, domains);
    }
  });
}

function prepareDomainRegistry(domains) {
  const plain = domains.plain;
  const regex = domains.regex.map(regex => {
    return new RegExp(regex);
  });
  return { plain, regex };
}


async function initJs(domains) {
  const searchResultsNode = document.querySelector('#links');

  let previousResultCount = 0;

  const observer = new MutationObserver(() => {
    const newResultCount = searchResultsNode.childElementCount;
    if (newResultCount != previousResultCount && newResultCount > 0) {
      decorateResultsJs(domains);
    }
    previousResultCount = newResultCount;
  });
  if (searchResultsNode) {
    observer.observe(searchResultsNode, { childList: true, subtree: false, attributes: false, characterData: false });
  }
}

async function initHtml(domains) {
  const searchResultsNode = document.querySelector('#links');
  if (searchResultsNode) {
    decorateResultsHtml(domains)
  }
}

async function init() {
  const decorationDescriptors = [{
    url: "//duckduckgo.com",
    init: initJs,
  }, {
    url: "//html.duckduckgo.com",
    init: initHtml,
  }];
  const pageUrl = window.location.href;
  const desc = decorationDescriptors.find(desc => {
    return pageUrl.indexOf(desc.url) !== -1;
  });
  if (desc !== undefined) {
    const options = await retrieveOptions();
    return desc.init(prepareDomainRegistry(options.domains));
  }
}

init();
