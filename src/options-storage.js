import { storage } from 'webextension-polyfill';

export async function retrieveOptions() {
  const data = await storage.local.get('options');
  return (data && data.options) ? data.options : {};
}

export function storeOptions(options) {
  return storage.local.set({
    options: options
  });
}
